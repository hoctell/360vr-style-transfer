from models import VGGEncoder, VGGDecoder
import torch
import torch.nn as nn
import torchvision

class WCT(nn.Module):
    def __init__(self, device):
        super().__init__()
        self.device = device
        self.e = nn.ModuleList([VGGEncoder(i) for i in range(1, 5)])
        self.d = nn.ModuleList([VGGDecoder(i) for i in range(1, 5)])

    def transform(self, cont_img, styl_img):
        # Extract multiple feature for one forward
        #sF4, sF3, sF2, sF1 = self.e[3].forward_multiple(styl_img)
        sFs = [e(styl_img)[0] for e in self.e]

        Im = cont_img
        for i in reversed(range(4)):
            out = self.e[i](Im)
            sF = sFs[i].data.squeeze(0)
            cF = out[0].data.squeeze(0)
            csF = self.__feature_wct(cF, sF)
            Im = self.d[i](csF, *out[1:])

        return Im

    def __feature_wct(self, cont_feat, styl_feat):
        cont_c, cont_h, cont_w = cont_feat.size(0), cont_feat.size(1), cont_feat.size(2)
        styl_c, styl_h, styl_w = styl_feat.size(0), styl_feat.size(1), styl_feat.size(2)
        cont_feat_view = cont_feat.view(cont_c, -1).clone().float()
        styl_feat_view = styl_feat.view(styl_c, -1).clone().float()
        target_feature = self.__wct_core(cont_feat_view, styl_feat_view)

        target_feature = target_feature.view_as(cont_feat)
        ccsF = target_feature.float().unsqueeze(0)
        return ccsF

    def __wct_core(self, cont_feat, styl_feat):
        c_mean = torch.mean(cont_feat, 1)  # c x (h x w)
        c_mean = c_mean.unsqueeze(1).expand_as(cont_feat)
        cont_feat = cont_feat - c_mean

        s_mean = torch.mean(styl_feat, 1)
        styl_feat = styl_feat - s_mean.unsqueeze(1).expand_as(styl_feat)

        whiten_cF = self.whitening_transform(cont_feat)
        targetFeature = self.coloring_transform(whiten_cF, styl_feat)
        targetFeature = targetFeature + s_mean.unsqueeze(1).expand_as(targetFeature)
        return targetFeature

    def whitening_transform(self, cont_feat):
        cFSize = cont_feat.size()

        iden = torch.eye(cFSize[0]) # identity matrix
        if self.device == 'cuda' and torch.cuda.is_available():
            iden = iden.cuda()

        contentConv = torch.mm(cont_feat, cont_feat.t()).div(cFSize[1] - 1) + iden # covariance matrix
        # Singular Value Decomposition
        c_u, c_e, c_v = torch.svd(contentConv, some=False)

        k_c = cFSize[0]
        for i in range(cFSize[0] - 1, -1, -1):
            if c_e[i] >= 0.00001:
                k_c = i + 1
                break

        c_d = (c_e[0:k_c]).pow(-0.5)
        step1 = torch.mm(c_v[:, 0:k_c], torch.diag(c_d))
        step2 = torch.mm(step1, (c_v[:, 0:k_c].t()))
        whiten_cF = torch.mm(step2, cont_feat)
        return whiten_cF

    def coloring_transform(self, whiten_cF, styl_feat):
        sFSize = styl_feat.size()

        styleConv = torch.mm(styl_feat, styl_feat.t()).div(sFSize[1] - 1) # covariance matrix
        # Singular Value Decomposition
        s_u, s_e, s_v = torch.svd(styleConv, some=False)

        k_s = sFSize[0]
        for i in range(sFSize[0] - 1, -1, -1):
            if s_e[i] >= 0.00001:
                k_s = i + 1
                break

        s_d = (s_e[0:k_s]).pow(0.5)
        targetFeature = torch.mm(torch.mm(torch.mm(s_v[:, 0:k_s], torch.diag(s_d)), (s_v[:, 0:k_s].t())), whiten_cF)
        return targetFeature

class PhotoWCT(WCT):
    def __init__(self, device):
        super().__init__(device)

from PIL import Image
import torchvision.transforms as transforms
import torchvision.utils as utils

def memory_limit_image_resize(cont_img):
    # prevent too small or too big images
    MINSIZE=256
    MAXSIZE=960
    orig_width = cont_img.width
    orig_height = cont_img.height
    if max(cont_img.width,cont_img.height) < MINSIZE:
        if cont_img.width > cont_img.height:
            cont_img.thumbnail((int(cont_img.width*1.0/cont_img.height*MINSIZE), MINSIZE), Image.BICUBIC)
        else:
            cont_img.thumbnail((MINSIZE, int(cont_img.height*1.0/cont_img.width*MINSIZE)), Image.BICUBIC)
    if min(cont_img.width,cont_img.height) > MAXSIZE:
        if cont_img.width > cont_img.height:
            cont_img.thumbnail((MAXSIZE, int(cont_img.height*1.0/cont_img.width*MAXSIZE)), Image.BICUBIC)
        else:
            cont_img.thumbnail(((int(cont_img.width*1.0/cont_img.height*MAXSIZE), MAXSIZE)), Image.BICUBIC)
    print("Resize image: (%d,%d)->(%d,%d)" % (orig_width, orig_height, cont_img.width, cont_img.height))
    return cont_img.width, cont_img.height

if __name__ == '__main__':
    #vgg16 = models.vgg16(pretrained=True)
    #pretrained_dict = vgg16.state_dict()
    #print(pretrained_dict.keys())

    model = torch.load('./PhotoWCTModels/photo_wct.pth')
    #print(model.keys())
    device = 'cpu'

    with torch.no_grad():

        photo_wct = PhotoWCT(device)
        #print(photo_wct.state_dict().keys())

        #state_dict = model.state_dict()
        new_state_dict = dict()
        for name, param in model.items():
            new_name = name
            for j, prefix_name in enumerate(['e%d' % i for i in range(1,6)]):
                new_name = new_name.replace(prefix_name, 'e.%d' % j)
            for j, prefix_name in enumerate(['d%d' % i for i in range(1,6)]):
                new_name = new_name.replace(prefix_name, 'd.%d' % j)
            #print(name, new_name)
            new_state_dict[new_name] = param
        photo_wct.load_state_dict(new_state_dict)


        cont_img = Image.open('images/s1.jpg').convert('RGB')
        styl_img = Image.open('images/style/20180912_1617.png').convert('RGB')

        cont_img = transforms.ToTensor()(cont_img).unsqueeze(0)
        styl_img = transforms.ToTensor()(styl_img).unsqueeze(0)
        if device == 'cuda' and torch.cuda.is_available():
            cont_img = cont_img.cuda()
            styl_img = styl_img.cuda()
            photo_wct.cuda()

        cont_img = torch.nn.functional.pad(cont_img, pad=(100, 100, 100, 100), mode='reflect')
        stylized_img = photo_wct.transform(cont_img, styl_img)

        utils.save_image(stylized_img.data.cpu().float(), 'test.png', nrow=1, padding=(-100, -100, -100, -100))
