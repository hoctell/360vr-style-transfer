import os
import torch
import torch.optim as optim
import torch.nn.functional as F
from torchvision import datasets, transforms
from photo_wct import PhotoWCT
from loader import Dataset

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

class Trainer():
    def __init__(self, model, args):
        self.const_model = model
        self.args = args

    def loss(self, encoder, content_img, style_img):
        mse_loss = torch.nn.MSELoss()
        reconst_img = self.const_model.transform(content_img, style_img)
        reconst_loss = mse_loss(reconst_img, content_img)
        reconst_feature = encoder(reconst_img)
        reconst_feature = reconst_feature[0] if isinstance(reconst_feature, tuple) else reconst_feature
        content_feature = encoder(content_img)
        content_feature = content_feature[0] if isinstance(content_feature, tuple) else content_feature
        feature_loss = mse_loss(reconst_feature, content_feature)
        print(reconst_loss.item(), feature_loss.item())
        return reconst_loss + feature_loss

    def train(self):
        torch.multiprocessing.set_start_method('spawn')
        dataset = Dataset(self.args.contentPath, self.args.stylePath)
        train_loader = torch.utils.data.DataLoader(dataset,
            batch_size=self.args.batch_size, shuffle=True, num_workers=1)
        for e in self.const_model.e:
            for param in e.parameters():
                param.requires_grad = False
        optimizers = [optim.SGD(model.parameters(), lr=self.args.lr, momentum=self.args.momentum) for model in self.const_model.d]
        for epoch in range(1, self.args.epochs + 1):
            self.train_epoch(epoch, train_loader, optimizers)

    def train_epoch(self, epoch, data_loader, optimizers):
        models = self.const_model.d
        for model in models:
            models.train()
        pid = os.getpid()
        for batch_idx, (content, style) in enumerate(data_loader):
            for i, optimizer in enumerate(optimizers):
                optimizer.zero_grad()
            loss = self.loss(self.const_model.e[i], content, style)
            loss.backward()
            for i, optimizer in enumerate(optimizers):
                optimizer.step()
            if batch_idx % self.args.log_interval == 0:
                print('{}\tTrain Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    pid, epoch, batch_idx * len(content), len(data_loader.dataset),
                    100. * batch_idx / len(data_loader), loss.item()))

def test(args, model):
    torch.manual_seed(args.seed)

    dataset = Dataset(args.contentPath,args.stylePath)
    test_loader = torch.utils.data.DataLoader(dataset,
        batch_size=args.batch_size, shuffle=True, num_workers=1)

    test_epoch(model, test_loader)

def train_epoch(epoch, args, models, data_loader, optimizers):
    model.train()
    pid = os.getpid()
    for batch_idx, (content, style) in enumerate(data_loader):
        for i, optimizer in enumerate(optimizers):
            optimizer.zero_grad()
            loss = loss(models[i], content, style)
            loss.backward()
            optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('{}\tTrain Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                pid, epoch, batch_idx * len(data), len(data_loader.dataset),
                100. * batch_idx / len(data_loader), loss.item()))


def test_epoch(model, data_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in data_loader:
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.max(1)[1] # get the index of the max log-probability
            correct += pred.eq(target).sum().item()

    test_loss /= len(data_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(data_loader.dataset),
100. * correct / len(data_loader.dataset)))

if __name__ == '__main__':
    model = torch.load('./PhotoWCTModels/photo_wct.pth')
    new_state_dict = dict()
    for name, param in model.items():
        new_name = name
        for j, prefix_name in enumerate(['e%d' % i for i in range(1,6)]):
            new_name = new_name.replace(prefix_name, 'e.%d' % j)
        for j, prefix_name in enumerate(['d%d' % i for i in range(1,6)]):
            new_name = new_name.replace(prefix_name, 'd.%d' % j)
        #print(name, new_name)
        new_state_dict[new_name] = param
    photo_wct = PhotoWCT('cuda')
    if torch.cuda.is_available():
        photo_wct = photo_wct.cuda()
    photo_wct.load_state_dict(new_state_dict)
    trainer = Trainer(photo_wct, AttrDict({
        'contentPath': 'data/val2017',
        'stylePath': 'images/style/20180912_1617.png',
        'batch_size': 1,
        'epochs': 1,
        'lr': 0.0001,
        'momentum': 0.09,
        'log_interval': 1,
    }))
    trainer.train()
